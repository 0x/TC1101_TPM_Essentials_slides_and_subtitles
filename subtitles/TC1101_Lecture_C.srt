1
00:00:00,880 --> 00:00:06,480
Welcome to this OST2 lecture about how
to set up your lab environment for the

2
00:00:06,480 --> 00:00:11,960
exercises in this course. We provide
ready to use environment using Docker.

3
00:00:11,960 --> 00:00:16,480
And we recommend this environment
because this protects you from breaking

4
00:00:16,480 --> 00:00:22,840
your own system if it already has a TPM.
If you're using a work machine or you're

5
00:00:22,840 --> 00:00:27,439
a student and your machine is being
managed by the organization then it is

6
00:00:27,439 --> 00:00:33,320
very likely that the TPM is already in
use by Microsoft Windows or by Linux.

7
00:00:33,320 --> 00:00:38,640
And we don't want you to have problems doing
this course. Therefore we recommend using

8
00:00:38,640 --> 00:00:44,960
Docker locally on your machine. It is
very easy to use and we provide a

9
00:00:44,960 --> 00:00:50,600
container that is ready to be downloaded
and just run on your local system.

10
00:00:50,600 --> 00:00:54,800
All the instructions are available in the
course. There is a third option for

11
00:00:54,800 --> 00:00:59,480
running the exercise and this is with a
hardware TPM. This is of course possible, just

12
00:00:59,480 --> 00:01:04,080
be aware aware of the risks. For most of
the exercises we will be using the tpm2-tools.

13
00:01:04,080 --> 00:01:09,640
For when we get behind the scenes
and see how actually the TPM operations

14
00:01:09,640 --> 00:01:15,280
happen, using an API from a TPM software
stack, then we'll discuss more about the

15
00:01:15,280 --> 00:01:20,799
software stack of choice. For now you
just need the tpm2-tools if you decide

16
00:01:20,799 --> 00:01:27,079
to use a hardware TPM. Here is a typical
workflow when using our Docker

17
00:01:27,079 --> 00:01:34,280
environment locally. On the next slide will
show a video demo of this exact flow.

18
00:01:34,280 --> 00:01:40,040
First we need to instruct Docker to
fetch our image from the Docker hub. This

19
00:01:40,040 --> 00:01:44,880
image already exists, this container. It's
being downloaded, then an image is

20
00:01:44,880 --> 00:01:50,759
created. Once the image is created the
container is actually being run and we

21
00:01:50,759 --> 00:01:55,920
are attaching an interactive terminal so
we can issue our commands. The first one

22
00:01:55,920 --> 00:02:01,240
being a TPM server. The TPM server is
actually the IBM TPM simulator made by

23
00:02:01,240 --> 00:02:07,399
Ken Goldman. The TPM log can be observed
for how the TPM simulator works. Usually

24
00:02:07,399 --> 00:02:11,640
we don't have to do that. But it's there
if you decide to do so. The very first

25
00:02:11,640 --> 00:02:16,680
command we issue to the TPM is startup.
If this was a real physical system the

26
00:02:16,680 --> 00:02:22,959
startup command would already be issued
by the UEFI or BIOS or the just the first

27
00:02:22,959 --> 00:02:27,280
firmware that initializes the system. In
this case, because it's a simulator, we

28
00:02:27,280 --> 00:02:32,200
just started it, we need to do the
command ourselves. And there is an easy

29
00:02:32,200 --> 00:02:38,560
to use tool to do that just don't forget
the dash lowercase c argument there. Once

30
00:02:38,560 --> 00:02:43,120
the TPM is started up we can start issuing
any TPM command and as many TPM commands

31
00:02:43,120 --> 00:02:48,920
as you like. The TPM to startup command
is needed only after the TPM server is

32
00:02:48,920 --> 00:02:53,480
started. After that it's no longer needed
it's only one time execution. For this

33
00:02:53,480 --> 00:02:58,879
example, to verify that the TPM simulator
is working, we're just going to generate

34
00:02:58,879 --> 00:03:03,239
some random bytes.
To be exact four random bytes and we're

35
00:03:03,239 --> 00:03:08,440
going to use the tpm2 as a source
of randomness. After that we'll print the

36
00:03:08,440 --> 00:03:14,799
stored file and we'll see that we get
four bytes. So let's see the demo.

37
00:03:20,319 --> 00:03:27,080
Here we already downloaded the
Docker container that's available in the

38
00:03:27,080 --> 00:03:33,959
Docker hub. And our local Docker has
created an image based on that container

39
00:03:33,959 --> 00:03:39,319
with some extra parameters that you'll
see more instructions in the course.

40
00:03:39,319 --> 00:03:44,159
Then we run the TPM server, we issued the
startup command, we ask the TPM to

41
00:03:44,159 --> 00:03:49,120
generate us four bytes, we received the
bytes. And we can see that we got the

42
00:03:49,120 --> 00:03:56,280
first byte is BB the second one is 93
this is in hex third one is 50 and the

43
00:03:56,280 --> 00:04:02,200
last one is E7. This is just an example
of how how easy it is to use the Docker

44
00:04:02,200 --> 00:04:07,319
environment from OST2 for this course. It
is up to you to decide do you prefer

45
00:04:07,319 --> 00:04:12,079
hardware TPM or a software TPM with our
Docker environment. And there's also an

46
00:04:12,079 --> 00:04:15,840
option to run it in a browser so we
don't have to install anything. More

47
00:04:15,840 --> 00:04:21,840
information you can find in the course
itself. Now the local setup has only one

48
00:04:21,840 --> 00:04:27,840
major requirement and this is to be able
to run a fairly recent version of Docker.

49
00:04:27,840 --> 00:04:33,280
Then there are several recommended
options to make your experience better.

50
00:04:33,280 --> 00:04:38,440
Most importantly the tpm2-tools need to
know how to talk with the TPM simulator.

51
00:04:38,440 --> 00:04:43,000
And for that there is an option you can
set this option once you are inside the

52
00:04:43,000 --> 00:04:47,960
Docker container with your interactive
shell. Or set it as an argument as shown

53
00:04:47,960 --> 00:04:52,120
on the screen. This command is also
available in the course description when

54
00:04:52,120 --> 00:04:55,840
we talk about the local setup. And I
prefer this way because once this is set

55
00:04:55,840 --> 00:05:00,120
on the Docker command line and it's true
for the complete execution of the

56
00:05:00,120 --> 00:05:06,280
container until we exit. And this makes
it very easy and just simplifies

57
00:05:06,280 --> 00:05:11,000
using the tpm2-tools. Now the other
important option that depends on your

58
00:05:11,000 --> 00:05:19,160
system, is to specify that the container
we provide is for Linux and x86 64bit

59
00:05:19,160 --> 00:05:24,440
system. This is particularly useful and
needed if you're using an ARM based

60
00:05:24,440 --> 00:05:29,840
Apple MacBook M1 M2 M3 or some of the
newer generations that will be coming

61
00:05:29,840 --> 00:05:34,680
out. And of course you need some kind of
a terminal application to operate with

62
00:05:34,680 --> 00:05:40,360
Docker. Docker itself offers a GUI that
has a way to provide your interactive

63
00:05:40,360 --> 00:05:44,600
shell. However I recommend having a
separate terminal application. One is

64
00:05:44,600 --> 00:05:50,319
provided nowadays with Windows, Linux, and
on Macs. So just look in your

65
00:05:50,319 --> 00:05:56,319
application menu you would find it.
Then we have the next one which is a

66
00:05:56,319 --> 00:06:02,720
really I think neat application of
Docker. This is a website that generates

67
00:06:02,720 --> 00:06:10,080
on demand instance of Docker. And we can
run up to 4 hours. There's no limit as

68
00:06:10,080 --> 00:06:15,880
far as I could see when using this
solution that is supported by Docker

69
00:06:15,880 --> 00:06:19,919
itself. They're offsetting the cost of
the website the hosting and the and the

70
00:06:19,919 --> 00:06:23,759
resources it uses. So you can just go
there and log in with your Docker

71
00:06:23,759 --> 00:06:27,720
account. Typically this requires no
charge. It's a free of charge account

72
00:06:27,720 --> 00:06:33,599
from hub.docker.com. I find it very useful
for testing for doing the exercises. It's

73
00:06:33,599 --> 00:06:38,919
rather quick once you've downloaded
the container from the hub. It just

74
00:06:38,919 --> 00:06:45,360
starts very fast and the resources
available are plenty for our needs.

75
00:06:45,360 --> 00:06:49,520
There are limitations as I mentioned one
Docker instance at a time so if you're

76
00:06:49,520 --> 00:06:53,840
doing something let's say we're doing
one exercise and you want to start doing

77
00:06:53,840 --> 00:06:57,759
another one in parallel this wouldn't be
possible. Also there's the limit of 4

78
00:06:57,759 --> 00:07:02,440
hours that I never could really hit and
even one or two times when I just

79
00:07:02,440 --> 00:07:06,960
forgot the window open and it run out I
was just able to close that instance and

80
00:07:06,960 --> 00:07:11,160
start a new instance. So this 4 hour
limit is more of a timeout rather than

81
00:07:11,160 --> 00:07:16,080
an actual limitation per day. So you can
use this environment for more than 4

82
00:07:16,080 --> 00:07:20,720
hours you would just need to restart
your instance. And our exercises really

83
00:07:20,720 --> 00:07:26,639
do not require like a one exercise alone
wouldn't take up to 4 hours. Depending on

84
00:07:26,639 --> 00:07:31,319
the exercise it could take 5, 10 minutes
or more. But it it wouldn't be 4 hours in

85
00:07:31,319 --> 00:07:37,440
any case. So this is a really good
solution and it is something I often

86
00:07:37,440 --> 00:07:43,639
use to try out different things with the
TPM simulator. Not to forget the same

87
00:07:43,639 --> 00:07:47,479
options would be required here as
well because this is just Docker

88
00:07:47,479 --> 00:07:51,440
running on someone else's computer. And
you're having the visual interface

89
00:07:51,440 --> 00:07:54,720
the terminal in front of you. So just don't forget the options from the

90
00:07:54,720 --> 00:07:59,319
previous slide. If you decide to use a
hardware TPM you just need to install the

91
00:07:59,319 --> 00:08:05,360
tpm2-TSS software stack and the tpm2-tools. Both are open source and have very

92
00:08:05,360 --> 00:08:10,840
good instructions for all of the popular
operating systems how to install.

93
00:08:10,840 --> 00:08:16,560
Just remember that on a Mac environment
you would need the TPM simulator. Or you

94
00:08:16,560 --> 00:08:23,440
can get a TPM to go on a USB stick, from Let's Trust. Now because of these

95
00:08:23,440 --> 00:08:27,960
complications and risk of breaking your
machine again I would recommend using

96
00:08:27,960 --> 00:08:32,640
the Docker environment. If if you want to
use a hardware TPM using our Docker image

97
00:08:32,640 --> 00:08:39,599
would not benefit you greatly. You
could in theory connect the hardware TPM with the

98
00:08:39,599 --> 00:08:43,839
Docker image, somehow forward the
communication, but it's not really

99
00:08:43,839 --> 00:08:47,800
beneficial. So if you decide to use the
hardwear TPM just make sure to install the

100
00:08:47,800 --> 00:08:52,959
software and be aware that your system
might not boot if it's being managed by

101
00:08:52,959 --> 00:08:57,399
your organization. If it's your personal
machine then again please check first

102
00:08:57,399 --> 00:09:02,200
your operating system if it has any
extra security feature enabled that is

103
00:09:02,200 --> 00:09:07,800
already using the TPM. And that's it. A
typical lab workflow is shown here.

104
00:09:07,800 --> 00:09:14,079
The descriptions are made to be
intuitive. We're not making

105
00:09:14,079 --> 00:09:19,399
hardly technical descriptions, but rather
an example would be create a digital

106
00:09:19,399 --> 00:09:24,720
signature using the TPM. Or seal a secret
using the TPM. And based on the

107
00:09:24,720 --> 00:09:29,320
information that you would hear and
learn from the lectures you have all the

108
00:09:29,320 --> 00:09:33,600
information necessary to solve these
challenges. Once you read the description

109
00:09:33,600 --> 00:09:37,200
the recommendation is to go and try to
solve it by yourself. If you're

110
00:09:37,200 --> 00:09:43,079
experience difficulty then we usually
put all the important tpm2-tools

111
00:09:43,079 --> 00:09:47,839
links. Let's say for the digital
signature it would be a link to tpm2_sign, 

112
00:09:47,839 --> 00:09:53,680
and the tpm2_verify tools, where you
can read about the different options. And

113
00:09:53,680 --> 00:09:56,880
by reading through you would probably
remember something we said during the

114
00:09:56,880 --> 00:10:01,920
lecture and be "Oh okay, so I see this is
the option I need, this is what I need to

115
00:10:01,920 --> 00:10:06,399
instruct the TPM to do". Now once you've
done that and you've tried again and if

116
00:10:06,399 --> 00:10:10,240
you if you're still experiencing
difficulties, which is very possible if

117
00:10:10,240 --> 00:10:16,399
your a newcomer beginner, the TPM has
complexity has a high know-how barrier, this

118
00:10:16,399 --> 00:10:21,360
is why we created these courses, to help
lower that barrier, that's totally okay

119
00:10:21,360 --> 00:10:26,680
we're providing solution videos in each
of the labs each exercise after its

120
00:10:26,680 --> 00:10:30,839
description after the links to the tools
the documentation for the specific

121
00:10:30,839 --> 00:10:35,639
exercise we have a solution video that
shows you exactly how we envision to

122
00:10:35,639 --> 00:10:39,519
solve this problem. And then I would
really suggest don't be satisfied with

123
00:10:39,519 --> 00:10:44,040
just seeing how it should work try out
the the instructions and if you have any

124
00:10:44,040 --> 00:10:49,360
issues just let us know in the OST2
discussion box. Each exercise each

125
00:10:49,360 --> 00:10:53,920
unit with an exercise has discussion box
feel free to drop any questions there.

126
00:10:53,920 --> 00:10:58,240
And of course experiment further once
you get it right once you get it working.

127
00:10:58,240 --> 00:11:02,680
And if you want want to do something
more we'll be happy to try to answer any

128
00:11:02,680 --> 00:11:07,959
questions about continuing the workflow of
a specific exercise, making more of

129
00:11:07,959 --> 00:11:12,160
the digital signature, or something else.
We recommend using the OST2 discussion

130
00:11:12,160 --> 00:11:16,800
board for all kind of questions. We just
recommend doing it below the section

131
00:11:16,800 --> 00:11:21,320
that created you the question so we have
more context especially when it is about

132
00:11:21,320 --> 00:11:26,120
exercises commenting after the exercise
would help us greatly understand the

133
00:11:26,120 --> 00:11:30,680
trouble you're having with a specific
section. And if you need an email for

134
00:11:30,680 --> 00:11:34,399
communication you can always reach us at
the course

135
00:11:34,399 --> 00:11:37,399
email

